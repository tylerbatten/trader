#An introduction to high frequency cryptocurrency trading on the Binance marketplace

from datetime import datetime
from tqdm import tqdm
import time
import pandas as pd
from heapq import nlargest
from binance.client import Client
key = ''
secret = ''
client = Client(key, secret)
precision_df = pd.DataFrame.from_csv('precisions.txt', sep='\t')
all_prices = client.get_all_tickers()

def start():
    global target_key 
    asset_balance = client.get_asset_balance(asset='BTC')
    BTC_balance = float(asset_balance['free'])
    if BTC_balance < 0.001: #should try for 30 seconds before canceling and calling start again
        print('BTC_balance less than 0.001')
        print(f'limit order made on {target_key}. Waiting 30s for completion... {datetime.now()}')
        time.sleep(30)
        if BTC_balance < 0.001:
            orders = client.get_open_orders(target_key+'BTC') #open orders
            order_id = orders[0]['orderId']
            client.cancel_order(symbol=target_key+'BTC', orderId=order_id)
            print(f'successfully cancelled {target_key} order.')
            start()
    else:
        pass
    all_vol = client.get_ticker()
    symbols = [d['symbol'] for d in all_vol]
    volume = [c['quoteVolume'] for c in all_vol]
    BTC_book = {}
    count = 0
    for s,v in zip(symbols,volume):
        if s[-3:] == 'BTC':
            BTC_book[s] = float(v)
            count += 1
    BTC_top_vol = nlargest(15,BTC_book,key=BTC_book.get)
    print(f'Iterating Top 15 Volumes on the BTC base market {datetime.now()}')
    rstd = []
    for coin in tqdm(BTC_top_vol):
        df = pd.DataFrame()
        key = coin[:-3]
        klines = client.get_historical_klines(key+'BTC', Client.KLINE_INTERVAL_1MINUTE, "15 minutes ago UTC")
        if len(klines) < 1:
            pass
        else:
            df = pd.DataFrame(klines)
            high = df[2].astype(float)
            med = high.median()
            std = high.std()
            val = std/med*100
            volume = df[10].astype(float).median() # Taker buy quote asset volume
            rstd.append({
                'coin':key,
                'rstd':val, 
                'vol':volume,
                'med': med,
                'std': std
            })
    rstd_dict = {}
    buy_dict = {}
    for i in rstd:
        rstd_dict[i['coin']] = i['rstd']*i['vol']
        buy_dict[i['coin']] = i['rstd']
    target_key = max(rstd_dict, key=rstd_dict.get)
    target_val = max(rstd_dict.values())
    coeff = max(buy_dict.values())/100
    print(coeff)
    buy_values = next(item for item in rstd if item['coin'] == target_key)
    median_price = buy_values['med']
    x = next(item for item in all_prices if item['symbol'] == target_key+'BTC')
    current_price = float(x['price'])
    change = round(float((current_price-median_price)/median_price*100), 2)
    precision = 8
    bp = round(min(current_price,median_price),8)
    buy_price = '{:0.0{}f}'.format(bp, precision)
    sp = round(bp*(1 + coeff),8)
    sell_price = '{:0.0{}f}'.format(sp, precision)
    precise_decimal = precision_df.loc[f'{target_key}/BTC', 'PRECISION']
    if precise_decimal == 1:
        tbb = int(BTC_balance/bp)
    else:
        tbb = round(BTC_balance/bp, precise_decimal)
    tbbs = '{:0.0{}f}'.format(tbb, precise_decimal)
    if change <= (-coeff+0.001)*100 and change >= (-coeff-0.001)*100:
        print('Limit buy order made: symbol= {}, quantity= {}, price {}. {}'.format(target_key+'BTC',tbbs,buy_price, datetime.now()))
        client.order_limit_buy(
            symbol=target_key+'BTC',
            quantity=tbbs,
            price=buy_price
        )
        return sell_check(target_key, sell_price, tbb, precise_decimal)
    else:
        print('Price of {} has strayed {} percent from the mean. Trying again. Yield would be {}. {}'.format(target_key, change,float(coeff*100), datetime.now()))
        print(f'To place order, change needs to be less than {(-coeff+0.001)*100} and greater than {(-coeff-0.001)*100}')
        return start()

def sell_check(target_key, sell_price, tbb, precise_decimal):
    coin_balance = client.get_asset_balance(asset=target_key)
    balance = float(coin_balance['free'])
    tbs = tbb-(tbb*0.001)
    tbss = '{:0.0{}f}'.format(balance, precise_decimal)
    if balance >= float(tbs):
        print('Limit sell order made: symbol= {}, quantity= {}, price= {}. {}'.format(target_key+'BTC', tbss, sell_price, datetime.now()))
        client.order_limit_sell(
            symbol=target_key+'BTC',
            quantity=tbss,
            price=sell_price
        )
        return start()
    else:
        time.sleep(2)
        print(f'trying to buy {target_key} for {sell_price} BTC via limit order. {datetime.now()}')
        return sell_check(target_key, sell_price, tbb, precise_decimal)

start()